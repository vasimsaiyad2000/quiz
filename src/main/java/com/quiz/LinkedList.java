package com.quiz;

public class LinkedList {

    private int counter;
    private Node head;
    
    public void add(int value) {
        if (head == null) {
            head = new Node(value);
        }
        
        Node temp = new Node(value);
        Node current = head;
        
        if (current != null) {
            while (current.getNext() != null) {
                current = current.getNext();
            }
            
            current.setNext(temp);
        }
        
        counter++;
    }
    
    public boolean remove() {
        Node current = head;

        if (head != null) {
            for (int i = 0; i < size() - 1; i++) {
                if (current.getNext() == null)
                    return false;
 
                current = current.getNext();
            }
            
            current.setNext(current.getNext().getNext());
            counter--;    
            
            return true;
 
        }
        
        return false;
    }
    
    public boolean removeGreaterThan(int value) {
        Node current = head;
        boolean isRemoved = false;
        
        while (current.getNext() != null) {
            if (current.getNext().getValue() > value) {
                current.setNext(current.getNext().getNext());
                counter--;
                isRemoved = true;
            }
            
            if (!isRemoved) {
                current = current.getNext();    
            }
            
            isRemoved = false;
        }
        
        return isRemoved;
    }
    
    public int size() {
        return counter;
    }
    
    public String toString() {
        String output = "";
 
        if (head != null) {
            Node current = head.getNext();
            while (current != null) {
                output += "[" + current.getValue() + "]";
                current = current.getNext();
            }
        }
        
        return output;
    }
    
}
