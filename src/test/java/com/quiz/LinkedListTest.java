package com.quiz;

import org.junit.Assert;
import org.junit.Test;

public class LinkedListTest {

    @Test
    public void addTest() {
        LinkedList list = new LinkedList();
        
        list.add(1);
        list.add(5);
        list.add(10);
        
        Assert.assertEquals(3, list.size());
    }
    
    @Test
    public void removeTest() {
        LinkedList list = new LinkedList();
        list.add(1);
        list.add(5);
        list.add(10);
        
        boolean value = list.remove();

        Assert.assertEquals(2, list.size());
        Assert.assertEquals(true, value);
        
    }
    
    @Test
    public void removeGreaterThanTest() {
        LinkedList list = new LinkedList();
        list.add(1);
        list.add(5);
        list.add(10);
        
        list.removeGreaterThan(1);
        Assert.assertEquals(1, list.size());
    }
}
